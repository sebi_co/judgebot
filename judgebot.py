#! python2

import os
import codecs
import sys
import time
import datetime
from docx import Document
from docx.text.paragraph import Paragraph
from docx.oxml.xmlchemy import OxmlElement

#######################################################################################################################
## Requirements:
## python 2 (python-docx would not install on python 3)
## python-docx: pip install python-docx
## pyinstaller: pip install pyinstaller
##  - used for creating exe file: pyinstaller --onefile judgebot.py
#######################################################################################################################
## What this script does:
##  1. creates a copy of the ecris file in output folder, with the same name as ecris file and a suffix with date
##  2. finds all case ids from ecris file
##  3. parses all .docx files in output folder (newest to oldest) and checks the case ids
##  4. if a case id from ecris file is found in an output file, some text is copied to the newly created file:
##  4a. first, some text is copied from output file (from old_file_copy_start to old_file_copy_end)
##  4b. then, some text is copied from template file (from template_copy_start to template_copy_end)
##  4c. the case id found is flagged as done, in order to not find it again in older output files
##  5. if case id is not found in any file, the whole template file is copied instead
##
## How to use:
##  1. configure judgebot_config.txt
##  2. have the ecris_file and template_file in judgebot directory
##  3. run executable (judgebot.exe) or script (judgebot.py - needs python 2 installed)
##  4a. if judgebot_error.txt exists, an error occured (check it and fix it)
##  4b. if judgebot_error.txt doesn't exist, the resulting docx should be found in output directory
##      with the input file name + date from ecris file + suffix (ex: file_18.04.2016_jb.docx)
##  5. do any changes on the resulting output file, and leave it in the output folder for future runs
#######################################################################################################################
## Author: Sebastian Copacian
## Revision Dates
## 1.0 (30.05.2018)
##  - Created script.
## 1.1 (14.06.2018)
##  - When searching case ids in the ecris file, added a new check for the previous
##    cell's text to be of the form '<number>.', to correspond to 'Nr. crt.' column.
##    This fixes a bug that causes the script to search and find case ids in the output files
##    instead of ecris file, when a paragraph is found in an output file that matches a case id.
## 1.2 (25.09.2018)
##  - Filter file names that start with '~', which are temporary files or OS protected files.
##  - Added an error message if no ecris file is found.
##  - The resulting file name now contains the date found in the input file, before the suffix.
##  - Output files are now checked in reverse chronological order based on date from file names,
##    instead of the file creation dates.
## 2.0 (24.10.2018)
##  - Changed the way case ids are searched, so that output files are only opened once.
##  - Added time spent at the end.
##  - If an output file is newer than the ecris file, a warning is issued.
##  - All output files older than the year specified by output_old_threshold are ignored.
## 2.1 (12.01.2020)
##  - Updated 'How to use' section and config file descriptions
##  - Added a template file and an example ecris file to the zip file
##  - No longer saving template file when closing it (no more errors if template file is open)
## 2.2 (26.05.2021)
##  - Changed how the date string is parsed in the ecris file
## 2.3 (2.09.2021)
##  - Allow whitespace when detecting case_ids
##  
#######################################################################################################################

start_time = time.time()

#######################################################################################################################
## inputs
#######################################################################################################################

config_file = 'judgebot_config.txt'

#######################################################################################################################
## methods
#######################################################################################################################

#######################################################################################################################
# get all files from a folder that contain a string in their name, except files that start with '~'
#######################################################################################################################
def get_files(path = '.', ext = 'docx'):
   a = os.listdir(path)
   return [x for x in a if ext in x and x[0] != '~']

#######################################################################################################################
# sort the files from a list in order of date creation
#######################################################################################################################
def sort_file_by_creation_date(file_list):
   date_list = []
   for file_i, file in enumerate(file_list):
      date_list.append((os.path.getmtime(file), file))
   date_list = sorted(date_list)
   #print(date_list)
   return [x[1] for x in date_list]

#######################################################################################################################
# sort the files from a list in order of date from the file name
#######################################################################################################################
def sort_file_by_date_from_name(file_list):
   date_list = []
   for file_i, file in enumerate(file_list):
      date = file.split('_jb')[0].split('_')[1]
      date_list.append((date, file))
   date_list = sorted(date_list)
   return [x[1] for x in date_list]

#######################################################################################################################
# get the date in the form of '18 APRILIE 2016', make it '2016.04.18'
#######################################################################################################################
def format_date_for_file_name(date):
   date = date.replace('IANUARIE',   '01')
   date = date.replace('FEBRUARIE',  '02')
   date = date.replace('MARTIE',     '03')
   date = date.replace('APRILIE',    '04')
   date = date.replace('MAI',        '05')
   date = date.replace('IUNIE',      '06')
   date = date.replace('IULIE',      '07')
   date = date.replace('AUGUST',     '08')
   date = date.replace('SEPTEMBRIE', '09')
   date = date.replace('OCTOMBRIE',  '10')
   date = date.replace('NOIEMBRIE',  '11')
   date = date.replace('DECEMBRIE',  '12')
   date = date.replace(' ', '.')
   date = date.split('.')[2] + '.' + date.split('.')[1] + '.' + date.split('.')[0] # DD.MM.YYYY -> YYYY.MM.DD
   return '_' + date

#######################################################################################################################
# save error in error file
#######################################################################################################################
def report_error(error, error_file):
   f = codecs.open(error_file, 'w', 'utf8')
   f.write(error)
   f.close()
   print(error.encode('utf-8'))
   print('Press <Enter> to finish...')
   raw_input()
   sys.exit()

#######################################################################################################################
# copy a run to an existing paragraph
#######################################################################################################################
def copy_run_to_para(run, paragraph):
   # create new run in paragraph with text to be copied
   new_run = paragraph.add_run(run.text)
   new_run.font.name = run.font.name
   new_run.font.size = run.font.size
   new_run.font.color.rgb = run.font.color.rgb
   new_run.bold = run.bold
   new_run.italic = run.italic
   new_run.underline = run.underline
   new_run.style.name = run.style.name

#######################################################################################################################
# copy a paragraph (src_para) after an existing para (after_para)
#######################################################################################################################
def copy_para_after_para(src_para, after_para):
   # copy the settings of the source paragraph
   new_para = insert_para_after(after_para)
   new_para.paragraph_format.alignment          = src_para.paragraph_format.alignment
   new_para.paragraph_format.first_line_indent  = src_para.paragraph_format.first_line_indent
   new_para.paragraph_format.keep_together      = src_para.paragraph_format.keep_together
   new_para.paragraph_format.keep_with_next     = src_para.paragraph_format.keep_with_next
   new_para.paragraph_format.left_indent        = src_para.paragraph_format.left_indent
   new_para.paragraph_format.line_spacing       = src_para.paragraph_format.line_spacing
   new_para.paragraph_format.line_spacing_rule  = src_para.paragraph_format.line_spacing_rule
   new_para.paragraph_format.page_break_before  = src_para.paragraph_format.page_break_before
   new_para.paragraph_format.right_indent       = src_para.paragraph_format.right_indent
   new_para.paragraph_format.space_after        = src_para.paragraph_format.space_after
   new_para.paragraph_format.space_before       = src_para.paragraph_format.space_before
   new_para.paragraph_format.widow_control      = src_para.paragraph_format.widow_control
   new_para.style.name                          = src_para.style.name

   # copy all runs from source paragraph
   for run_i, run in enumerate(src_para.runs):
      copy_run_to_para(run, new_para)

#######################################################################################################################
# Insert a new paragraph after the given paragraph (googled)
#######################################################################################################################
def insert_para_after(paragraph, text=None, style=None):
    new_p = OxmlElement("w:p")
    paragraph._p.addnext(new_p)
    new_para = Paragraph(new_p, paragraph._parent)
    if text:
        new_para.add_run(text)
    if style is not None:
        new_para.style = style
    return new_para


#######################################################################################################################
# get the contents of the template file from a specified start to a specified end
#######################################################################################################################
def get_template_data(template_file, template_start='*', template_end='*'):
   # open the template file
   try:
      template_doc = Document(template_file)
   except:
      report_error('ERROR: Cannot open file: %s! Make sure it is closed!' % template_file, error_file)
   template_paragraphs = template_doc.paragraphs

   template_data = []
   copy_en = False

   # start with last paragraph because paragraphs are copied after existing paragraph
   for para_i, para in enumerate(reversed(template_paragraphs)):

      # copy a new paragraph as long as copy_en flag is true
      if copy_en == True:
         template_data.append(para)
         # found the start of text to be copied (included)
         if template_start in para.text:
            return template_data

      # found the end of the text to be copied (excluded)
      if template_end in para.text or template_end == '*':
         copy_en = True

   # if the return above was not reached and template_start/end are not '*', something went wrong
   if copy_en == False:
      report_error('ERROR: template_copy_end = \'%s\' was not found in template_file = %s!'
         % (template_end, template_file), error_file)
   elif template_start != '*':
      report_error('ERROR: template_copy_start = \'%s\' was not found in template_file = %s!'
         % (template_start, template_file), error_file)

   # closing template file with save doesn't seem to be needed, but causes error if template file is open
   #template_doc.save(template_file)
   return template_data

#######################################################################################################################
# copy a list of paragraphs in a doc at a certain location
#######################################################################################################################
def copy_paras_to_a_doc(paragraph_list, new_output_file, case_id):
   # open the new output file
   try:
      new_doc = Document(new_output_file)
   except:
      report_error('ERROR: Cannot open file: %s! Make sure it is closed!' % new_output_file, error_file)

   found_case_id = None
   tables = new_doc.tables
   for table in tables:
      for row in table.rows:
         for cell_i, cell in enumerate(row.cells):
            for paragraph_i, paragraph in enumerate(cell.paragraphs):

               # found the case_id in this file
               if paragraph.text.strip() == case_id:
                  found_case_id = case_id

               # find first cell where text needs to be added for a case_id
               if found_case_id is not None and add_text_after_field in paragraph.text.strip():
                  for para in reversed(paragraph_list):
                     copy_para_after_para(src_para=para, after_para=paragraph)
                  found_case_id = None

   # create new output file
   new_doc.save(new_output_file)

#######################################################################################################################
## 1. read configuration file
#######################################################################################################################

f = codecs.open(config_file, 'r', 'utf8')
for line in f:
   if 'ecris_file' in line and line[0] != '#':
      ecris_file = line.split('=')[1].strip()
      print('Looking for ecris_file: %s.' % ecris_file.encode('utf-8'))

   if 'template_file' in line and line[0] != '#':
      template_file = line.split('=')[1].strip()
      print('Looking for template_file: %s.' % template_file.encode('utf-8'))

   if 'add_text_after_field' in line and line[0] != '#':
      add_text_after_field = line.split('=')[1].strip()
      print('Add text after field: %s.' % add_text_after_field.encode('utf-8'))

   if 'old_file_copy_start' in line and line[0] != '#':
      old_file_copy_start = line.split('=')[1].strip()
      print('Old file copy start: %s.' % old_file_copy_start.encode('utf-8'))

   if 'old_file_copy_end' in line and line[0] != '#':
      old_file_copy_end = line.split('=')[1].strip()
      print('Old file copy end: %s.' % old_file_copy_end.encode('utf-8'))

   if 'template_copy_start' in line and line[0] != '#':
      template_copy_start = line.split('=')[1].strip()
      print('Template copy start: %s.' % template_copy_start.encode('utf-8'))

   if 'template_copy_end' in line and line[0] != '#':
      template_copy_end = line.split('=')[1].strip()
      print('Template copy end: %s.' % template_copy_end.encode('utf-8'))

   if 'output_path' in line and line[0] != '#':
      output_path = line.split('=')[1].strip()
      print('Output file is saved in: /%s.' % output_path.encode('utf-8'))

   if 'output_file_suffix' in line and line[0] != '#':
      output_file_suffix = line.split('=')[1].strip()
      print('Output file suffix is: %s.' % output_file_suffix.encode('utf-8'))

   if 'case_id_separator' in line and line[0] != '#':
      case_id_separator = line.split('=')[1].strip()
      print('Case id separator is: %s.' % case_id_separator.encode('utf-8'))

   if 'case_id_characters' in line and line[0] != '#':
      case_id_characters = line.split('=')[1].strip()
      print('Case id characters (besides digits) are: %s.' % case_id_characters.encode('utf-8'))

   if 'output_old_threshold' in line and line[0] != '#':
      output_old_threshold = line.split('=')[1].strip()
      print('Ignore output files older than: %s.' % output_old_threshold.encode('utf-8'))

   if 'error_file' in line and line[0] != '#':
      error_file = line.split('=')[1].strip()
      print('Errors will be saved in: %s.' % error_file.encode('utf-8'))
f.close

# delete old error file
if error_file in get_files(path = '.', ext = '.'):
   os.remove(error_file)

#######################################################################################################################
## 2. check if all files/folders exist
#######################################################################################################################

# create output folder if it doesn't exist
if not os.path.exists(output_path):
   os.makedirs(output_path)

# if name of specified ecris_file is '*', get the latest .docx file
if ecris_file.rsplit('.', 1)[0] == '*':
   possible_ecris_files = get_files(path = '.', ext = '.docx')
   # remove template_file from possible ecris files
   if template_file in possible_ecris_files:
      possible_ecris_files.remove(template_file)
   if len(possible_ecris_files) == 0:
      report_error('ERROR: Cannot find an ecris file in /judgebot/ folder!', error_file)
   ecris_file = sort_file_by_creation_date(possible_ecris_files)[-1]
#print('ecris file is', ecris_file)
# check if ecris file exists
if ecris_file not in get_files(path = '.', ext = '.docx'):
   report_error('ERROR: Cannot find ecris file specified: %s!' % ecris_file, error_file)

# check if template_file exists
if template_file not in get_files(path = '.', ext = '.docx'):
   report_error('ERROR: Cannot find template_file specified: %s!' % template_file, error_file)

# get a list of all output files, sorted by the date included in their name
output_files = get_files(path = output_path, ext = '.docx')
output_files_new = [x for x in output_files if x.split('_jb')[0].split('_')[1] >= output_old_threshold]
output_files_sorted = sort_file_by_date_from_name([output_path + '/' + x for x in output_files_new])
new_output_file = ""

#######################################################################################################################
## 3. get all case ids from ecris file
#######################################################################################################################

case_id_list = []
court_date_start = 'LA DATA DE '

try:
   doc = Document(ecris_file)
except:
   report_error('ERROR: Cannot open file: %s! Make sure it is closed!' % ecris_file, error_file)

tables = doc.tables
for table in tables:
   for row in table.rows:
      for cell_i, cell in enumerate(row.cells):
         for paragraph_i, paragraph in enumerate(cell.paragraphs):

            # get the calendar date of the court hearing from the ecris file
            if 'court_date' not in globals() and court_date_start in paragraph.text:
               court_date = paragraph.text.split(court_date_start)[1]
               court_date = court_date.split()
               court_date = court_date[0:3]
               court_date = ' '.join(court_date)
               court_date = format_date_for_file_name(court_date)

               # create the name of the new output file
               new_output_file = output_path + '/' + ecris_file.rsplit('.', 1)[0] + court_date + output_file_suffix

               # check if an output file already exists
               if new_output_file in output_files_sorted:
                  report_error('ERROR: Output file \'%s\' already exists in /%s/ folder! Remove it first!'
                     % (new_output_file, output_path), error_file)

               # check if there are output files newer than ecris file
               for output_file in output_files_sorted:
                  if output_file.split('_jb')[0].split('_')[1] > court_date[1:]:
                     print('\nWARNING: ecris file is older than output file: %s!' % output_file)
                     print('Press <Enter> to continue anyway...')
                     raw_input()

            # find valid case_id
            old_split = old_paragraph.split('.') if 'old_paragraph' in globals() else ''
            # if the text consists of 3 numbers separated by case_id_separator and the cell number is 1
            if (len(paragraph.text.split(case_id_separator)) == 3
            and all(x == case_id_separator or x.isdigit() or x in case_id_characters for x in paragraph.text.strip())
            and cell_i == 1 and old_split[0].isdigit() and old_split[1] == ''):
               # found new valid case_id
               case_id_list.append(paragraph.text.strip())

            old_paragraph = paragraph.text

# create new output file
doc.save(new_output_file)

print('\n%s case ids were found in ecris file %s.' % (len(case_id_list), ecris_file))

#######################################################################################################################
## 4. search all case ids in all output files
#######################################################################################################################

print('\nParse all output files in reversed chronological order...')

template_paragraphs_found_ids = get_template_data(template_file, template_copy_start, template_copy_end)

for output_file in reversed(output_files_sorted):
   print('\nSearching output file: %s.' % output_file)
   try:
      jb_doc = Document(output_file)
   except:
      report_error('ERROR: Cannot open file: %s! Make sure it is closed!' % output_file, error_file)

   found_case_id = None
   copy_en = False
   paragraph_list = []

   tables = jb_doc.tables
   for table_i, table in enumerate(tables):
      for row_i, row in enumerate(table.rows):
         for cell_i, cell in enumerate(row.cells):
            for paragraph_i, paragraph in enumerate(cell.paragraphs):

               # search all (remaining) case ids in this output file
               for case_id in case_id_list:
                  # 1. found the case_id in this file
                  if paragraph.text.strip() == case_id:

                     # if case_id was found at this point, something went wrong
                     if found_case_id is not None:
                        if copy_en is False:
                           report_error('ERROR: old_file_copy_start = \'%s\' was not found in file %s!'
                                 % (old_file_copy_start, output_file), error_file)
                        else:
                           report_error('ERROR: old_file_copy_end = \'%s\' was not found in file %s!'
                                 % (old_file_copy_end, output_file), error_file)

                     found_case_id = case_id
                     print(' :) Case id %s found!' % found_case_id)

               # 2. found the start of the text to be copied
               if found_case_id is not None and old_file_copy_start in paragraph.text:
                  copy_en = True

               # 3. found the end of the text to be copied
               if copy_en is True and old_file_copy_end in paragraph.text:
                  # append the part of template file that is copied for found case ids 
                  # (reverse back, since reverse is done again by copy_paras_to_a_doc() )
                  paragraph_list += reversed(template_paragraphs_found_ids)
                  copy_paras_to_a_doc(paragraph_list, new_output_file, found_case_id)

                  # remove case id that is done and reset variables for next case id
                  case_id_list.remove(found_case_id)
                  found_case_id = None
                  copy_en = False
                  paragraph_list = []

               # 4. add new paragraph to list
               if copy_en is True:
                  paragraph_list.append(paragraph)

   # if case_id was found at this point, something went wrong
   if found_case_id is not None:
      if copy_en is False:
         report_error('ERROR: old_file_copy_start = \'%s\' was not found in file %s!'
               % (old_file_copy_start, output_file), error_file)
      else:
         report_error('ERROR: old_file_copy_end = \'%s\' was not found in file %s!'
               % (old_file_copy_end, output_file), error_file)

#######################################################################################################################
## 5. handle case ids not found in output files
#######################################################################################################################

if len(case_id_list) > 0:
   print('\nThe following case ids were not found in any of the output files:')

   template_paragraphs_not_found_ids = get_template_data(template_file, template_start='*', template_end='*')

   # open the new output file
   try:
      new_doc = Document(new_output_file)
   except:
      report_error('ERROR: Cannot open file: %s! Make sure it is closed!' % new_output_file, error_file)

   found_case_id = None
   tables = new_doc.tables
   for table in tables:
      for row in table.rows:
         for cell_i, cell in enumerate(row.cells):
            for paragraph_i, paragraph in enumerate(cell.paragraphs):

               # search all (remaining) case ids in new output file
               for case_id in case_id_list:
                  # found the case_id in this file
                  if paragraph.text.strip() == case_id:
                     found_case_id = case_id
                     print(' :( Case id %s NOT found!' % case_id)

               # find first cell where text needs to be added for a case_id
               if found_case_id is not None and add_text_after_field in paragraph.text:
                  for para in template_paragraphs_not_found_ids:
                     copy_para_after_para(src_para=para, after_para=paragraph)
                  found_case_id = None

   # create new output file
   new_doc.save(new_output_file)

# Finished
time_spent = datetime.timedelta(seconds = int(time.time() - start_time))
print('\nJob done! Time spent = %s.' % time_spent)
print('Your output file is here: %s/%s' % (output_path, new_output_file))
print('Press <Enter> to finish...')
raw_input()
